import { createStore } from 'vuex'

const state = {
  rows: []
}

const getters = {
  getRows: (state) => state.rows
}

const actions = {
  setRow(context) {
    context.commit('SET_ROW')
  },

  setUpvoteNumber(context, rowId) {
    context.commit('SET_UPVOTE_NUMBER', rowId)
  },

  toggleRow(context, rowId) {
    context.commit('TOGGLE_ROW', rowId)
  },
}

const mutations = {
  SET_ROW(state) {
    const id = state.rows.length

    state.rows.push({
      id: id,
      selected: false,
      upvoteNumber: 0
    })
  },

  SET_UPVOTE_NUMBER(state, rowId) {
    const row = state.rows.find(row => row.id === rowId)
    row.upvoteNumber++
  },

  TOGGLE_ROW(state, rowId) {
    const row = state.rows.find(row => row.id === rowId)
    row.selected = !row.selected
  }
}

const store = createStore({
  state,
  getters,
  actions,
  mutations
})

export default store